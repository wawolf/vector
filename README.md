# V E C T O R #

### What is Vector? ###
Vector is an open-source *JavaScript* library which simplifies coordinates computation.
It provides simple arithmetic operations:
* Addition
* Substraction
* Multiplication
* Division
As well as cloning for usability purposes.

### How do we use Vector? ###
Using Vector is easy (yeah, surprising!).

Here are some ways to create a Vector:

```javascript
var pointA = new Vector(1, 2)
var pointB = new Vector2D(1, 2)
var pointC = new Vector([1, 2])
var pointC = new Vector2D({x: 1, y: 2})
```