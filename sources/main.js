!function(){
	var lastInstance = 0
	Vector = function(){
		var array = []
		this.instance = lastInstance + 1

		for (var i = 0; i < arguments.length; i++)
			array.push(arguments[i])

		if (array[0] && typeof array[0].length === 'number')
			array = array[0]

		for (var i = 0; i < array.length; i++)
			this[i] = array[i]

		this.__defineGetter__('length', function(){
			return array.length
		})

		this.__defineGetter__('x', function(){
			return this[0]
		})
		this.__defineGetter__('y', function(){
			return this[1]
		})
		this.__defineGetter__('z', function(){
			return this[2]
		})

		var self = this
		this.toString = function(){
			if (!array.length)
				return ''

			var str = self[0]
			for (var i = 1; i < array.length; i++)
				str += ',' + self[i]
			return str
		}
	}

	Vector.prototype.clone = function(){
		return new Vector(this)
	}

	Vector.prototype.add = function(v, createNew){
		if (arguments.length > 1 && typeof arguments[1] === 'number'){
			var result = this

			if (typeof arguments[arguments.length - 1] !== 'number' && arguments[arguments.length - 1])
				result = this.clone()

			var maxFar = Math.min(result.length, arguments.length - (typeof arguments[arguments.length - 1] === 'number'? 0 :1))
			for (var i = 0; i < maxFar; i++)
				result[i] += arguments[i]

			return result
		}

		var result = createNew? this.clone() :this

		if (typeof v === 'number'){
			for (var i = 0; i < result.length; i++)
				result[i] += v
		}
		else{
			var maxFar = Math.min(result.length, v.length)
			for (var i = 0; i < maxFar; i++)
				result[i] += v[i]
		}

		return result
	}

	Vector.prototype.less = function(v, createNew){
		if (arguments.length > 1 && typeof arguments[1] === 'number'){
			var result = this

			if (typeof arguments[arguments.length - 1] !== 'number' && arguments[arguments.length - 1])
				result = this.clone()

			var maxFar = Math.min(result.length, arguments.length - (typeof arguments[arguments.length - 1] === 'number'? 0 :1))
			for (var i = 0; i < maxFar; i++)
				result[i] -= arguments[i]

			return result
		}

		var result = createNew? this.clone() :this

		if (typeof v === 'number'){
			for (var i = 0; i < result.length; i++)
				result[i] -= v
		}
		else{
			var maxFar = Math.min(result.length, v.length)
			for (var i = 0; i < maxFar; i++)
				result[i] -= v[i]
		}

		return result
	}

	Vector.prototype.mul = function(v, createNew){
		if (arguments.length > 1 && typeof arguments[1] === 'number'){
			var result = this

			if (typeof arguments[arguments.length - 1] !== 'number' && arguments[arguments.length - 1])
				result = this.clone()

			var maxFar = Math.min(result.length, arguments.length - (typeof arguments[arguments.length - 1] === 'number'? 0 :1))
			for (var i = 0; i < maxFar; i++)
				result[i] += arguments[i]

			return result
		}

		var result = createNew? this.clone() :this

		if (typeof v === 'number'){
			for (var i = 0; i < result.length; i++)
				result[i] *= v
		}
		else{
			var maxFar = Math.min(result.length, v.length)
			for (var i = 0; i < maxFar; i++)
				result[i] *= v[i]
			for (; i < result.length; i++)
				result[i] = 0
		}

		return result
	}

	Vector.prototype.div = function(v, createNew){
		var result = createNew? this.clone() :this

		if (typeof v === 'number'){
			for (var i = 0; i < result.length; i++)
				result[i] /= v
		}
		else{
			var maxFar = Math.min(result.length, v.length)
			for (var i = 0; i < maxFar; i++)
				result[i] /= v[i]
			for (; i < result.length; i++)
				result[i] = result[i] >= 0? Infinity :-Infinity
		}

		return result
	}
}();